<?php
  include_once $_SERVER['DOCUMENT_ROOT'] . '/koneksi.php';
  include_once $_SERVER['DOCUMENT_ROOT'] . '/conf.php';

  define('ITEM_PER_PAGE', 20);

  function countByKeyword($db, $tbname, $keyword) {
    $result = $db->query("SELECT COUNT(*) as total FROM $tbname where nama LIKE '%$keyword%'");
    if (!$result) {
      die('Error: Fail count table ' . $tbname);
    }
    $result = $result->fetch_array();
    return $result['total'];
  }

  function currentPaging($db, $tbname, $keyword) {
    $total = countByKeyword($db, $tbname, $keyword);
    $pages = ceil($total / ITEM_PER_PAGE);

    return [ 
      'total' => $total,
      'pages' => $pages
    ];
  }

  if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['command']) && $_GET['command'] == 'count') {
    if (empty($_GET['tbname'])) {
			http_response_code(500);   
      die('Error: property "tbname" is required');
    }
    $tbname = $_GET['tbname'];
    $keyword = '';
    if (isset($_GET['keyword'])) {
			$keyword = $_GET['keyword'];
    }
    $result = currentPaging($db, $tbname, $keyword);

    returnJson($result);
  }
?>